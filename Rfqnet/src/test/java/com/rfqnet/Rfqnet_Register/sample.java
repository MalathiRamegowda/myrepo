package com.rfqnet.Rfqnet_Register;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.rfqnet.Rfqnet_page_object.BaseClass;
import com.rfqnet.Rfqnet_page_object.CustomerLogin;
import com.rfqnet.Rfqnet_page_object.HomePage;

public class sample extends BaseClass {

	
	public static HomePage hmpge;
	public static CustomerLogin cl;
	 
	public static void main(String[] args) throws InterruptedException, AWTException {
		// TODO Auto-generated method stub
		String expectedTitle="Customer Login";
		String title2="Vendor Portal";
	
		try {
			driver=initializeDriver();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		driver.get(prop.getProperty("url"));
    	Thread.sleep(5000);
    	hmpge=new HomePage(driver);
       	WebElement we =hmpge.popUpNotification();
    	JavascriptExecutor js=(JavascriptExecutor)driver;
    	js.executeScript("arguments[0].click();", we);
    	webNotificationEnable();
		      	Thread.sleep(5000);
		      	WebElement register=hmpge.registerHomePage();
		      	js.executeScript("arguments[0].click();", register);
		      	
    	//hmpge.popUpNotification().click();
    	Thread.sleep(200);
    	cl=new CustomerLogin(driver);
    	cl.createAccountbtn().click();
        Select sc=new Select(cl.userRoledpd());
    	sc.selectByVisibleText(" Become Vendor");
    	//hmpge.popUpNotification().click();
    	Assert.assertEquals(driver.getTitle(), title2);

	}

	
	

}
