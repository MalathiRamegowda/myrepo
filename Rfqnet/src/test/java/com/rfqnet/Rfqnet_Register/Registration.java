package com.rfqnet.Rfqnet_Register;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.rfqnet.Rfqnet_page_object.BaseClass;
import com.rfqnet.Rfqnet_page_object.BuyerAccount;
import com.rfqnet.Rfqnet_page_object.CustomerLogin;
import com.rfqnet.Rfqnet_page_object.HomePage;
import com.rfqnet.Rfqnet_page_object.VendorPage;


public class Registration extends BaseClass {
	public String expectedTitle="Customer Login";
	public String title2="Vendor Portal";
	public String expectedmessage="Thank you for your interest";
	public static JavascriptExecutor js;
	public static HomePage hmpge;
	public static CustomerLogin cl;
	

	
	@BeforeMethod
	public void setup() throws IOException
	{
	
		driver=initializeDriver();
		driver.get(prop.getProperty("url"));

	}
	
	
	/*
	 * @Test public void register() throws AWTException, InterruptedException {
	 * 
	 * HomePage hmpge=new HomePage(driver); //Allow Web notification WebElement
	 * we=hmpge.popUpNotification(); elementClickJavascript(we); //notification
	 * webNotificationEnable(); WebElement register = hmpge.registerHomePage();
	 * elementClickJavascript(register); Thread.sleep(2000);
	 */
		  //verify the web page title
		/*
		 * String actualTitle = driver.getTitle(); System.out.println(actualTitle);
		 * Assert.assertEquals(actualTitle, expectedTitle);
		         }*/
        
	
	/*
	 * @Test(enabled=false) public void userRole() { CustomerLogin cl=new
	 * CustomerLogin(driver); cl.createAccountbtn().click();
	 * 
	 * }
	 */
	/*
	 * @Test(priority=3) public void returnLogIn() {
	 * 
	 * VendorPage vp=new VendorPage(driver); vp.returnlogin().click(); }
	 */
	  
	  
	  @Test
	  public void createVendor() throws AWTException, InterruptedException {
		  
		hmpge = new HomePage(driver); 
		hmpge.CreateAccount();
		Select sc=new Select(cl.userRoledpd());

		/*Register vendor */

		sc.selectByValue("2");
		//verifying page title
		Assert.assertEquals(driver.getTitle(), title2);
		//input field values for vendor
		VendorPage vp=new VendorPage(driver);
		vp.company().sendKeys("rfqsupplychaincompany"); vp.phoneNUm().sendKeys("6476785657");
		vp.email().sendKeys("rfqsupplychaincompany@gmail.com");
		Select shippingcar=new Select(vp.shippCarrier());
		shippingcar.selectByValue("fedex");
		vp.domainname().sendKeys("rfqsupplychaincompany"); vp.password().sendKeys("xyz123");
		vp.confirmPass().sendKeys("xyz123"); vp.contactName().sendKeys("rfqnet");
		vp.streetadd().sendKeys("wood st"); vp.streetadd2().sendKeys("1000");
		vp.city().sendKeys("Toronto"); vp.zipcode().sendKeys("M4Y 2P8");
		Select countrysel=new Select(vp.country());
		countrysel.selectByValue("CA"); Select
		regionsel=new Select(vp.regionid()); regionsel.selectByValue("74");
		vp.logo().sendKeys("C:\\Users\\sningego\\Documents\\rfqnet.docx");
		vp.register().click(); String message = vp.successreg().getText();
		Assert.assertEquals(message, expectedmessage);

	  
	  
	  
	  }
	  @Test
	  public void createBuyer() throws AWTException, InterruptedException {
	    hmpge=new HomePage(driver); 
		hmpge.CreateAccount();
	    cl=new CustomerLogin(driver);
		cl.createAccountbtn().click();
		Select sc=new Select(cl.userRoledpd());
		//Register vendor
		sc.selectByValue("1");
		BuyerAccount buyer=new BuyerAccount(driver);
		buyer.company().sendKeys("refqnetbfr");
		buyer.firstName().sendKeys("Johnmat");
		buyer.lastName().sendKeys("Mountgomoryabd");
		buyer.email().sendKeys("abcttu@gmail.com");
		buyer.password().sendKeys("123123");
		buyer.confirmPassword().sendKeys("123123");
		js.executeScript("window.scroll(0,400)");
		buyer.register().click();
		Thread.sleep(5000);

	  }
	
	 @AfterMethod
	 public void teardown() {
		 driver.close(); }
	
	 }
