package com.rfqnet.Rfqnet_page_object;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class BaseClass {

	public static WebDriver driver;
	public static Properties prop;
	public static JavascriptExecutor js=(JavascriptExecutor)driver;
public static WebDriver initializeDriver() throws IOException
{
	String path="C:\\Users\\sningego\\eclipse-workspace\\Rfqnet\\src\\main\\java\\Config\\Data.properties";
    prop= new Properties();
    FileInputStream fis=new FileInputStream(path);

    prop.load(fis);
    String browserName=prop.getProperty("browser");
    System.out.println(browserName);

if(browserName.equals("firefox"))
{
	 System.setProperty("webdriver.gecko.driver", "C:\\Users\\sningego\\eclipse-workspace\\Rfqnet\\Drivers\\geckodriver.exe");
	driver= new FirefoxDriver();
		//execute in chrome driver
	
}
else if (browserName.equals("chrome"))
{
	 driver= new ChromeDriver();
	//firefox code
}
else if (browserName.equals("IE"))
{
//	IE code
}

driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
return driver;
}

public Timeouts implicitWait()
{
	 return driver.manage().timeouts().implicitlyWait(400, TimeUnit.SECONDS);
	
}



public void getScreenshot(String result) throws IOException
{
	File src=((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(src, new File("C://test//"+result+"screenshot.png"));
	
}

public void alertAccept() {
	Alert a=driver.switchTo().alert();
    a.accept();
	
}

public void alertDismiss() {
	Alert a=driver.switchTo().alert();
  a.dismiss();
}

public static void webNotificationEnable() throws AWTException {
	Robot robot=new Robot();
	robot.keyPress(KeyEvent.VK_ALT);
	robot.keyPress(KeyEvent.VK_A);
	robot.keyRelease(KeyEvent.VK_A);
	robot.keyRelease(KeyEvent.VK_ALT);
	
}

public Object elementClickJavascript(WebElement Element) {
	
	 return js.executeScript("arguments[0].click();",Element); 
}

public Object elementScroll(int x, int y) {
	
	 // js.executeScript("window.scroll(0,400);", ""); //
	return js.executeScript("window.scrollBy(x,y)");
}

}
