package com.rfqnet.Rfqnet_page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CustomerLogin extends BaseClass {
	
WebDriver driver;
	
	public CustomerLogin(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	
	    @FindBy(xpath=".//a[@class='button']")
	    public WebElement CreateAcc;
	    @FindBy(xpath=".//Select[@class='form-control']")
	    public  WebElement UserRole;
	    @FindBy(xpath=".//button[@name='send']")
	    public WebElement Login;
	    
	    
	    public WebElement createAccountbtn()
	    {
	    	return CreateAcc;
	    }
	    public WebElement userRoledpd() {
	    	return UserRole;
	    }
	    
	    public WebElement loginbtn() {
	    	return Login;
	    }

}
