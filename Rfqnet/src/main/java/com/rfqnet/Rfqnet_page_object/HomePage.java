package com.rfqnet.Rfqnet_page_object;

import java.awt.AWTException;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage  extends BaseClass{
	 WebDriver driver;
	 public static JavascriptExecutor js;
	
	public HomePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver,this);
		
	}
	
	   
	    @FindBy(xpath="//li//a[contains(text(),'Register')]")
	    public WebElement register;
	    @FindBy(xpath=".//div/button[contains(text(),'Allow')]")
	    public WebElement popup;
	    
	    
	    public WebElement registerHomePage()
	    {
	    	return register;
	    }
         
	
	  public WebElement popUpNotification() {
		  return popup; }
	  
	  
	 public void CreateAccount() throws InterruptedException, AWTException {
	 
      //Allow Web notification
        WebElement we=popUpNotification();
        js=(JavascriptExecutor)driver;
        js.executeScript("arguments[0].click();", we);
		webNotificationEnable();
		
		 /*click on create Account*/
		 Thread.sleep(2000);
		 //js.executeScript("window.scroll(0,400)");
		 js.executeScript("window.scroll(0,400)");
		 WebElement register = registerHomePage();
		 js.executeScript("arguments[0].click();", register);
		 //wait
		 Thread.sleep(2000);	
		 
		 
         	 }
}
