package com.rfqnet.Rfqnet_page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VendorPage {
	
	WebDriver driver;
	
	
	   @FindBy(xpath="//input[@id='vendor_name']")
	   public  WebElement company;
	   @FindBy(xpath="//input[@id='telephone']")
	   public  WebElement phoneNUm;
	   @FindBy(xpath="//input[@id='email']")
	   public  WebElement email;
	   @FindBy(xpath="//select[@id='carrier_code']")
	   public  WebElement shippCarrier;
	   @FindBy(xpath="//input[@id='url_key']")
	   public  WebElement domainname;
	   @FindBy(xpath="//input[@id='password']")
	   public  WebElement password;
	   @FindBy(xpath="//input[@id='password_confirm']")
	   public  WebElement confirmPass;
	   @FindBy(xpath="//input[@id='vendor_attn']")
	   public WebElement contactName;
	   @FindBy(xpath="//input[@id='street1']")
	   public  WebElement streetadd;
	   @FindBy(xpath="//input[@id='street2']")
	   public  WebElement streetadd2;
	   @FindBy(xpath="//input[@id='city']")
	   public  WebElement city;
	   @FindBy(xpath="//input[@id='zip']")
	   public  WebElement zipcode;
	   @FindBy(xpath="//select[@id='country_id']")
	   public  WebElement country;
	   @FindBy(xpath="//select[@id='region_id']")
	   public  WebElement regionid;
	   @FindBy(xpath="//input[@id='logo']")
	   public  WebElement logo;
	   @FindBy(xpath="//button[@name='send']")
	   public WebElement register;
	   @FindBy(xpath="//div[@class='buttons-set']//a")
	   public WebElement returnlogin;
	   @FindBy(xpath="//span[@class='greenhulk']")
	   public WebElement successreg;

	public VendorPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver,this);
	}
	
	public WebElement company() {
		return company;
	}
	
	public WebElement phoneNUm() {
		return phoneNUm;
	}
	
	public WebElement email() {
		return email;
	}
	
	public WebElement shippCarrier() {
		return shippCarrier;
	}
	
	public WebElement domainname() {
		return domainname;
	}
	
	public WebElement password() {
		return password;
	}
	
	public WebElement confirmPass() {
		return confirmPass;
	}
	
	public WebElement contactName() {
		return contactName;
	}
	
	public WebElement streetadd() {
		return streetadd;
	}
	
	public WebElement streetadd2() {
		return streetadd2;
	}
	
	public WebElement city() {
		return city;
	}
	
	public WebElement zipcode() {
		return zipcode;
	}
	
	public WebElement regionid() {
		return regionid;
	}
	
	public WebElement logo() {
		return logo;
	}
	
	public WebElement country() {
		return country;
	}
	
	
	public WebElement register() {
		return register;
	}
	
	public WebElement returnlogin() {
		return returnlogin;
	}
	
	public WebElement successreg() {
		return successreg;
	}
	
	
}
