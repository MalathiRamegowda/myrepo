package com.rfqnet.Rfqnet_page_object;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BuyerAccount {
	
	WebDriver driver;
	
	@FindBy(xpath="//input[@id='company']")
	 public WebElement company;
	
	@FindBy(xpath="//input[@id='firstname']")
	 public WebElement firstName;
	
	@FindBy(xpath="//input[@id='lastname']")
	 public WebElement lastName;
	
	@FindBy(xpath="//input[@id='email_address']")
	 public WebElement email;
	
	@FindBy(xpath="//input[@id='password']")
	 public WebElement password;
	
	@FindBy(xpath="//input[@id='confirmation']")
	 public WebElement confirmPassword;
	
	@FindBy(xpath="//button[@title='Register']")
	 public WebElement register;

			
	
	public BuyerAccount(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public WebElement company() {
		return company;
	}
	
	public WebElement firstName() {
		return firstName;
	}
	
	public WebElement lastName() {
		return lastName;
	}
	
	public WebElement email() {
		return email;
	}
	
	public WebElement password() {
		return password;
	}
	
	public WebElement confirmPassword() {
		return confirmPassword;
	}
	
	public WebElement register() {
		return register;
	}


}
